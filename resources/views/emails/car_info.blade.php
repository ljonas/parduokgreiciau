<html lang="en-US">
<head>
    <meta charset="text/html">
</head>
<body>
    <ul>
        <h3>Naujas auto</h3>
        <li>
            Marke: {{$car_info['marke'] or '-'}}
        </li>
        <li>
            Modelis: {{$car_info['modelis'] or '-' }}
        </li>
        <li>
            Darbinis tūris: {{$car_info['turis'] or '-' }}
        </li>
        <li>
            Kuras: {{ $car_info['kuras'] or '-'  }}
        </li>
        <li>
            Metai: {{ $car_info['metai'] or '-'  }}
        </li>
        <li>
            Tech-apžiūra: {{ $car_info['tech'] or '-'  }}
        </li>
        <li>
            Kaina: {{ $car_info['kaina'] or '-'  }}
        </li>
        <li>
            Miestas: {{ $car_info['miestas'] or '-'  }}
        </li>
        <li>
            telefonas: {{ $car_info['telefonas'] or '-'  }}
        </li>
        <li>
            El. paštas: {{ $car_info['email'] or '-'  }}
        </li>
        <li>
            Komentaras: {{ $car_info['komentaras'] or '-'  }}
        </li>
        <li>
            Data: {{ $date or '-' }}
        </li>
    </ul>

    @if($photos != null)
        @foreach ($photos as $photo)
            @if($photo != null)
                <img src="<?php echo $message->embed($photo->getRealPath()); ?>" width="20%">
            @endif
        @endforeach
    @endif
</body>
</html>
