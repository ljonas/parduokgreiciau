<!DOCTYPE html>
<html>

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1, maximum-scale=1">
    <link href='https://fonts.googleapis.com/css?family=Lato:400,400italic,700,700italic,900,900italic,300italic&subset=latin,latin-ext' rel='stylesheet' type='text/css'>
    <link href="{{ URL::asset('assets/libraries/font-awesome-4.5.0/css/font-awesome.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('assets/libraries/owl.carousel/assets/owl.carousel.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('assets/libraries/colorbox/example1/colorbox.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('assets/libraries/bootstrap-select/bootstrap-select.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('assets/libraries/bootstrap-fileinput/fileinput.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('assets/css/superlist.css') }}" rel="stylesheet" type="text/css">

    <link rel="shortcut icon" type="image/x-icon" href="assets/favicon.png">

    <title>Automobiliu supirkimas</title>
</head>

<body>
<div class="page-wrapper">
    <div class="main">
        <div class="main-inner">
            <div class="content">
                <div class="mt-150">
                    <div class="hero-image">
                        <div class="hero-image-inner"
                             style="background-image: url('assets/img/tmp/slider-large-3.jpg');">
                            <div class="hero-image-content">
                                <div class="container">
                                    <div class="col-sm-6 main-text">
                                        <h1>AUTOMOBILIŲ SUPIRKIMAS</h1>
                                        <p>Nesivargink pardavinėti, priimk protingą sprendimą – pasiūlyk savo automobilį mums. Užpildyk formą ir mes su Jumis susisieksime.</p>
                                    </div>
                                </div>
                            </div>
                            <div class="hero-image-form-wrapper">
                                <div class="container">
                                    <div class="row">
                                        <div class="col-sm-6 col-sm-offset-6 col-lg-6 col-lg-offset-6">
                                            <form method="POST" action="/" accept-charset="UTF-8" enctype="multipart/form-data">
                                                {{ csrf_field() }}

                                                <h2>Siūlykite savo automobilį</h2>

                                                <div class='two-col'>
                                                    <div class="hero-image-keyword form-group">
                                                        <input type="text" name='marke' class="form-control"
                                                               placeholder="Markė" required>
                                                    </div>
                                                    <div class="hero-image-keyword form-group">
                                                        <input type="text" name='modelis' class="form-control"
                                                               placeholder="Modelis" required>
                                                    </div>
                                                </div>

                                                <div class='two-col'>
                                                    <div class="hero-image-keyword form-group">
                                                        <input type="text" name='turis' class="form-control"
                                                               placeholder="Darbinis tūris">
                                                    </div>
                                                    <div class="hero-image-location form-group">
                                                        <input type="text" name='metai' class="form-control"
                                                               placeholder="Metai" required>
                                                    </div>
                                                </div>


                                                <div class='two-col'>
                                                    <div class="hero-image-keyword form-group">
                                                        <select class="form-control" name='kuras' title="Kuro tipas">
                                                            <option>Benzinas</option>
                                                            <option>Dyzelinas</option>
                                                            <option>Benzinas/Dujos</option>
                                                        </select>
                                                    </div>
                                                    <div class="hero-image-location form-group">
                                                        <select class="form-control" name='tech'
                                                                title="Tech. apžiūra">
                                                            <option>Yra</option>
                                                            <option>Nėra</option>
                                                        </select>
                                                    </div>
                                                </div>

                                                <div class='two-col'>
                                                    <div class="hero-image-keyword form-group">
                                                        <input type="text" name='kaina' class="form-control"
                                                               placeholder="Pageidaujama suma">
                                                    </div>
                                                    <div class="hero-image-keyword form-group">
                                                        <input type="text" name='miestas' class="form-control"
                                                               placeholder="Miestas" required>
                                                    </div>
                                                </div>

                                                <div class='two-col'>
                                                    <div class="hero-image-keyword form-group">
                                                        <input type="text" name='telefonas' class="form-control"
                                                               placeholder="Telefonas" required>
                                                    </div>
                                                    <div class="hero-image-keyword form-group">
                                                        <input type="email" name='email' class="form-control"
                                                               placeholder="El. paštas">
                                                    </div>
                                                </div>

                                                <div class="hero-image-keyword form-group comment">
                                                    <input type="textbox" name='komentaras' class="form-control"
                                                           placeholder="Komentaras">
                                                </div>

                                                <div class="hero-image-keyword form-group photos-input">
                                                    <div class="input-group">
                                                        <input type="text" class="form-control" readonly="" class="file-names" placeholder="Nuotraukos">
                                                        <span class="input-group-btn">
                                                            <span class="btn btn-primary btn-file">
                                                                Pridėti <input multiple="1" name="images[]" type="file">
                                                            </span>
                                                        </span>
                                                    </div>
                                                </div>

                                                <button type="submit" class="btn btn-primary btn-block">Siūlyti</button>
                                            </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="container">

                    <div class="block background-secondary fullwidth">
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="contact-info-wrapper">
                                    <h2>Susisiekite su mumis dabar</h2>

                                    <div class="contact-info">
                                        <a class="contact-info-item" href="mailto:parduokgreiciau24@gmail.com">
                                            <i class="fa fa-at"></i>
                                            <span>parduokgreiciau24@gmail.com</span>
                                        </a>

                                        <a class="contact-info-item" href="tel:867942199">
                                            <i class="fa fa-phone"></i>
                                            <span>+370 67942199</span>
                                        </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="page-header">
                        <h1>Informacija</h1>

                        <p>Patikimas ir skubus automobilių supirkimas protingomis kainomis. Greitai perkame betkokių markių ir stovio automobilius,visoje Lietuvoje. Sutvarkome dokumentus vietoje, atsiskaitome IŠKARTO. Domina įvairūs pasiūlymai, galime pirkti daugiau automobilių iškarto.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <footer class="footer">
        <div class="footer-top">
            <div class="container">
                <div class="row">
                    <div class="col-sm-4">
                        <h2>Apie automobilių supirkimą</h2>
                        <p>Nupirksime betkokį automobilį už Jums priimtiną kaina.</p>
                    </div>

                    <div class="col-sm-4">
                        <h2>Kontaktine informacija</h2>
                        <p>
                            +370 67942199<br>
                            <a href="mailto:parduokgreiciau24@gmail.com">parduokgreiciau24@gmail.com</a>
                        </p>
                    </div>
                    <div class="col-sm-4">
                        <h2>Raskite mus facebooke</h2>

                        <ul class="social-links nav nav-pills">
                            <li><a href="https://www.facebook.com/Naudotos-automobiliu-dalys-271688239556332/"><i
                                            class="fa fa-facebook"></i></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-bottom">
            <div class="container">
                <div class="footer-bottom-right">
                    &copy; 2015 All rights reserved.
                </div>
            </div>
        </div>
    </footer>
</div>

<script src="assets/js/jquery.js" type="text/javascript"></script>
<script src="assets/js/map.js" type="text/javascript"></script>

<script src="assets/libraries/bootstrap-sass/javascripts/bootstrap/collapse.js" type="text/javascript"></script>
<script src="assets/libraries/bootstrap-sass/javascripts/bootstrap/carousel.js" type="text/javascript"></script>
<script src="assets/libraries/bootstrap-sass/javascripts/bootstrap/transition.js" type="text/javascript"></script>
<script src="assets/libraries/bootstrap-sass/javascripts/bootstrap/dropdown.js" type="text/javascript"></script>
<script src="assets/libraries/bootstrap-sass/javascripts/bootstrap/tooltip.js" type="text/javascript"></script>
<script src="assets/libraries/bootstrap-sass/javascripts/bootstrap/tab.js" type="text/javascript"></script>
<script src="assets/libraries/bootstrap-sass/javascripts/bootstrap/alert.js" type="text/javascript"></script>

<script src="assets/libraries/colorbox/jquery.colorbox-min.js" type="text/javascript"></script>

<script src="assets/libraries/flot/jquery.flot.min.js" type="text/javascript"></script>
<script src="assets/libraries/flot/jquery.flot.spline.js" type="text/javascript"></script>

<script src="assets/libraries/bootstrap-select/bootstrap-select.min.js" type="text/javascript"></script>

<script type="text/javascript" src="assets/libraries/owl.carousel/owl.carousel.js"></script>
<script type="text/javascript" src="assets/libraries/bootstrap-fileinput/fileinput.min.js"></script>

<script src="assets/js/superlist.js" type="text/javascript"></script>


<script type="text/javascript">
    $(document).on('change', '.btn-file :file', function() {
        var input = $(this),
                numFiles = input.get(0).files ? input.get(0).files.length : 1,
                label = input.val().replace(/\\/g, '/').replace(/.*\//, '');
        input.trigger('fileselect', [numFiles, label]);
    });

    $(document).ready( function() {
        $('.btn-file :file').on('fileselect', function (event, numFiles, label) {

            var input = $(this).parents('.input-group').find(':text'),
                    log = numFiles > 1 ? numFiles + ' files selected' : label;

            if (input.length) {
                input.val(log);
            } else {
                if (log) alert(log);
            }
        });


        $('form').submit(function (e) {
            e.preventDefault();
            var submit_button = $('.btn.btn-primary.btn-block');
            submit_button.prop('disabled', true);

            var fd = new FormData();
            var file_data = $('input[type="file"]')[0].files; // for multiple files
            for(var i = 0;i<file_data.length;i++){
                fd.append("images["+i+"]", file_data[i]);
            }
            var other_data = $('form').serializeArray();
            $.each(other_data,function(key,input){
                fd.append(input.name,input.value);
            });
            $.ajax({
                type: 'POST',
                url: '/',
                data: fd,
                processData: false,
                contentType: false
            })
            .done(function (data) {
                if(data=='OK'){
                    $('form').html('<h1>Dėkojame už užklausą</h1>');
                }else{
                    submit_button.html(data);
                    submit_button.prop('disabled', false);
                }
            }).fail(function (data) {
                $('form').html('<h1>Nepavyko išsiusti užklausos, bandykite vėliau</h1>');
            });
        });
    });

</script>

</body>
</html>
