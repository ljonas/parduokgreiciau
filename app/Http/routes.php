<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

Route::get('/', function () {
    return view('home');
});

Route::post('/', function (Request $request) {
    // getting all of the post data
    $files = Input::file('images');
    // Making counting of uploaded images
    $file_count = count($files);
    // start count how many uploaded
    $uploadcount = 0;

    if($file_count > 0){
        foreach($files as $file) {
            $rules = array('file' => 'image'); //'required|mimes:png,gif,jpeg,txt,pdf,doc'
            $validator = Validator::make(array('file'=> $file), $rules);
            if($validator->passes()){
                $uploadcount ++;
            }
        }
    }

    if($uploadcount == $file_count){
        $dateTimeNow = Carbon\Carbon::now()->toDateTimeString();

        $data = [
            'car_info' => $request->all(),
            'date' => $dateTimeNow,
            'photos' => $files
        ];

        Mail::send(['html'=>'emails.car_info'], $data, function ($message) {
            $message->to('parduokgreiciau24@gmail.com');
        });
        return 'OK';
    }
    else {
        return 'Galima prisegti tik nuotraukas';
    }
});